package com.atos.alfresco.docubase.webscript;

import java.util.HashMap;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;

public class CountDocuments extends DeclarativeWebScript {
	private final String PARAM_NODEREF = "noderef";
	private NodeService nodeService;
	private FileFolderService fileFolderService;
	private ContentService contentService;

	@Override
	protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
		Map<String, Object> model = new HashMap<String, Object>(1);
		
		// Récupération du nodeRef passé en paramètres
		Map<String,String> templateArgs = req.getServiceMatch().getTemplateVars();
		String nodeRefString = templateArgs.get(PARAM_NODEREF);
		NodeRef nodeRef = new NodeRef(nodeRefString);
		
		// Vérification de l'existance du dossier
		boolean exists = this.nodeService.exists(nodeRef);
		if (!exists) {
			status.setCode(Status.STATUS_NOT_FOUND,"Le dossier n'a pas été trouvé");
			status.setRedirect(false);
			model.put("size", 0);
		} else {
			int size = this.fileFolderService.listFiles(nodeRef).size();
			model.put("size", size);
			
			// Enregistrement dans un fichier dans le même dossier
			FileInfo newNodeInfo = this.fileFolderService.create(nodeRef, "Compteur-" + size, ContentModel.TYPE_CONTENT);
			ContentWriter writer = this.contentService.getWriter(newNodeInfo.getNodeRef(), ContentModel.PROP_CONTENT, true);
			writer.putContent("Il y a " + size + " documents.");
		}
		
		return model;
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	public void setFileFolderService(FileFolderService fileFolderService) {
		this.fileFolderService = fileFolderService;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}
	
	
}
