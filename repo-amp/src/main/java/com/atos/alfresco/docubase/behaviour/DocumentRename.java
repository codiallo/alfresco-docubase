package com.atos.alfresco.docubase.behaviour;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies.OnCreateNodePolicy;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;

import com.atos.alfresco.docubase.model.DocubaseModel;

public class DocumentRename implements OnCreateNodePolicy {
	static Logger log = Logger.getLogger(DocumentRename.class);

	private PolicyComponent policyComponent;
	private NodeService nodeService;
	
	public void init() {
		this.policyComponent.bindClassBehaviour(
				OnCreateNodePolicy.QNAME,				
				DocubaseModel.TYPE_CUSTOM_CONTENT,
				new JavaBehaviour(this, "onCreateNode", NotificationFrequency.TRANSACTION_COMMIT));
	}
	
	@Override
	public void onCreateNode(ChildAssociationRef childAssocRef) {		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = sdf.format(new Date());
		
		NodeRef nodeRef = childAssocRef.getChildRef();
		String name = (String) this.nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);
		
		log.debug("Renommage du document " + name);
		this.nodeService.setProperty(nodeRef, ContentModel.PROP_NAME, currentDate + "-" + name);
	}

	public void setPolicyComponent(PolicyComponent policyComponent) {
		this.policyComponent = policyComponent;
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	
	
}
