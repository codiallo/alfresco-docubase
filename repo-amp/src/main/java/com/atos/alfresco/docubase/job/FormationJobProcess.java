package com.atos.alfresco.docubase.job;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.nodelocator.NodeLocatorService;
import org.alfresco.repo.nodelocator.SharedHomeNodeLocator;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;


public class FormationJobProcess {
	static Logger log = Logger.getLogger(FormationJobProcess.class);
	private NodeLocatorService nodeLocatorService;
	private FileFolderService fileFolderService;
	private TransactionService transactionService;
	private String documentPrefix;
	
	public NodeRef run() {
		log.info("Nouvelle exécution du job");
		
		final NodeRef sharedFolderRef = nodeLocatorService.getNode(SharedHomeNodeLocator.NAME, null, null);
		
		final RetryingTransactionHelper transactionHelper = transactionService.getRetryingTransactionHelper();
		final RetryingTransactionCallback<NodeRef> callback = new RetryingTransactionCallback<NodeRef>() {
			@Override
			public NodeRef execute() throws Throwable {
				log.debug("Execution du traitement");
				FileInfo info = fileFolderService.create(sharedFolderRef, documentPrefix+" Mon Doc "+Math.random(), ContentModel.TYPE_CONTENT);
				return info.getNodeRef();
			}
		};
		
		//transactionHelper.doInTransaction(callback, false, false);
		
		NodeRef nodeRef = AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<NodeRef>()
        {
           public NodeRef doWork() throws Exception
           {
        	   log.debug("Appel du traitement");				
        	   NodeRef nodeRef = transactionHelper.doInTransaction(callback, false, false);
        	   return nodeRef;
           }
        }, AuthenticationUtil.getSystemUserName());
          
		return nodeRef;
	}

	public void setNodeLocatorService(NodeLocatorService nodeLocatorService) {
		this.nodeLocatorService = nodeLocatorService;
	}

	public void setFileFolderService(FileFolderService fileFolderService) {
		this.fileFolderService = fileFolderService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}
	
	public void setDocumentPrefix(String documentPrefix) {
		this.documentPrefix = documentPrefix;
	}
	
}
