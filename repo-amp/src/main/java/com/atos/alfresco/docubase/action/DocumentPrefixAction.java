package com.atos.alfresco.docubase.action;

import java.util.List;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;

public class DocumentPrefixAction extends ActionExecuterAbstractBase {
	public static final String NAME = "actionPrefixDocument";
	public static String PARAM_PREFIX="prefix";
	private NodeService nodeService;
	
	@Override
	protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {
		String prefix = (String) action.getParameterValue(PARAM_PREFIX);
		
		if (this.nodeService.exists(actionedUponNodeRef)) {
			String name = (String) this.nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_NAME);
			this.nodeService.setProperty(actionedUponNodeRef, ContentModel.PROP_NAME, prefix + "-" + name);
		}
	}

	@Override
	protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
		paramList.add(new ParameterDefinitionImpl(PARAM_PREFIX, DataTypeDefinition.TEXT, true, getParamDisplayLabel(PARAM_PREFIX), false, "ac-prefix"));
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	
}
