package com.atos.alfresco.docubase.model;

import org.alfresco.service.namespace.QName;

public interface DocubaseModel {
	public static final String DOCUBASE_MODEL_URI = "http://www.docubase.fr/model/content/1.0";
	
	static final QName TYPE_CUSTOM_CONTENT = QName.createQName(DOCUBASE_MODEL_URI, "customContent");
	static final QName ASPECT_STATUT = QName.createQName(DOCUBASE_MODEL_URI, "aspectStatut");
	
	static final QName PROP_STATUT = QName.createQName(DOCUBASE_MODEL_URI, "statut");
	static final QName PROP_STATUT2 = QName.createQName(DOCUBASE_MODEL_URI, "statut2");
    
}
