if (args.nodeRef !=null) {
  var dossier = search.findNode(args.nodeRef);
  if (dossier != null) {
	var childs = dossier.childFileFolders(true, false);
	model.enfants = childs;
  } else {
	status.code = 404;
	status.message = "Impossible de trouver le dossier";
	status.redirect = true;
  }
} else {
	status.code = 400;
	status.message = "Le node ref est obligatoire";
	status.redirect = true;
}