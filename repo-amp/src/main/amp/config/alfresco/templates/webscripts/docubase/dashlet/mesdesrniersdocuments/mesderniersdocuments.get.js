<import resource="classpath:/alfresco/templates/webscripts/org/alfresco/slingshot/documentlibrary/evaluator.lib.js">
<import resource="classpath:/alfresco/templates/webscripts/org/alfresco/slingshot/documentlibrary/parse-args.lib.js">

var userHomePath = userhome.getQnamePath();
var searchQuery = {
	query: "PATH:\""+userHomePath+"//*\" AND TYPE:\"cm:content\"",
	sort: [{
		column: "cm:created",
		ascending: false
	}],
	page: {
		maxItems: 5
	}
};

var nodes = search.query(searchQuery);

// Copi� de doclist.lib.js
var items = [];
var favourites = Common.getFavourites();
for each (node in nodes)
{
	// Get evaluated properties.
	item = Evaluator.run(node);
	if (item !== null)
	{
		item.isFavourite = (favourites[item.node.nodeRef] === true);
		item.likes = Common.getLikes(node);

			location =
			{
				file: node.name
			};
		
		
		// Resolved location
		item.location = location;		
		items.push(item);
	}
	else
	{
		--totalRecords;
	}
}
	
model.results = items;
