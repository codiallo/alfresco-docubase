<#assign nbEnfants = enfants?size>
Il y a ${nbEnfants} document<#if nbEnfants &gt; 1>s</#if>.<br />

Nom des documents : <br />
<#list enfants as result>
	${result.name}
	<#if result_has_next><br /></#if>
</#list>
