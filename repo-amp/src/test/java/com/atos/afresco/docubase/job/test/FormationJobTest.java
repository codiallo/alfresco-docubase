package com.atos.afresco.docubase.job.test;

import static org.junit.Assert.assertTrue;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atos.alfresco.docubase.job.FormationJobProcess;
import com.tradeshift.test.remote.Remote;
import com.tradeshift.test.remote.RemoteTestRunner;

@RunWith(RemoteTestRunner.class)
@Remote(runnerClass = SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:alfresco/application-context.xml")
public class FormationJobTest {

	@Autowired
	@Qualifier("formationJobProcess")
	FormationJobProcess process;
	
	@Autowired
	@Qualifier("nodeService")
	NodeService nodeService;
		
	@Test
	public void testJobExecution(){
		NodeRef nodeRef = process.run();
		
		// Récupération du nom pour vérification
		String name = (String) this.nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);
		
		// Le process crée un document avec un suffixe aléatoire, on ne peut donc tester que le préfixe.
		assertTrue(name.startsWith("PREFIX Mon Doc "));
	}
}
