package com.atos.afresco.docubase.action.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.nodelocator.NodeLocatorService;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atos.alfresco.docubase.action.DocumentPrefixAction;
import com.tradeshift.test.remote.Remote;
import com.tradeshift.test.remote.RemoteTestRunner;

@RunWith(RemoteTestRunner.class)
@Remote(runnerClass = SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:alfresco/application-context.xml")
public class DocumentPrefixTest {
	private static final String ADMIN_USER_NAME = "admin";
	
	@Autowired
	@Qualifier("ActionService")
	ActionService actionService;
	
	@Autowired
	@Qualifier("FileFolderService")
	protected FileFolderService fileFolderService;

	@Autowired
	@Qualifier("NodeService")
	protected NodeService nodeService;

	@Autowired
	@Qualifier("nodeLocatorService")
	NodeLocatorService nodeLocatorService;
	
	@Test
	public void testActionPrefix() {
		AuthenticationUtil.setFullyAuthenticatedUser(ADMIN_USER_NAME);

		// Récupération du dossier où classer le document
		NodeRef companyHome = nodeLocatorService.getNode("companyhome", null, null);
		assertNotNull(companyHome);

		// création du document
		String documentName = "MonDocument" + UUID.randomUUID();
		FileInfo documentInfo = this.fileFolderService.create(companyHome, documentName, ContentModel.TYPE_CONTENT);
		assertNotNull(documentInfo);
		
		// Vérification du nom (au cas où un behaviour serait exécuté)
		String newDocumentName = (String) this.nodeService.getProperty(documentInfo.getNodeRef(), ContentModel.PROP_NAME);
		assertNotNull(newDocumentName);
		
		Map<String, Serializable> params = new HashMap<String, Serializable>(1);
		params.put(DocumentPrefixAction.PARAM_PREFIX, "TEST");
		Action prefixAction = this.actionService.createAction(DocumentPrefixAction.NAME, params);
		this.actionService.executeAction(prefixAction, documentInfo.getNodeRef());
		
		// Récupération du nouveau nom
		String newNewDocumentName = (String) this.nodeService.getProperty(documentInfo.getNodeRef(), ContentModel.PROP_NAME);
		assertNotNull(newNewDocumentName);
		assertEquals("TEST-" + newDocumentName, newNewDocumentName);
		
	}
}
