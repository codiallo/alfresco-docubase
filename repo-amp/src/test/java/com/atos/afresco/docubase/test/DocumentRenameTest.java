package com.atos.afresco.docubase.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.nodelocator.NodeLocatorService;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atos.alfresco.docubase.model.DocubaseModel;
import com.tradeshift.test.remote.Remote;
import com.tradeshift.test.remote.RemoteTestRunner;

@RunWith(RemoteTestRunner.class)
@Remote(runnerClass = SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:alfresco/application-context.xml")
public class DocumentRenameTest {

	@Autowired
	@Qualifier("FileFolderService")
	protected FileFolderService fileFolderService;

	@Autowired
	@Qualifier("NodeService")
	protected NodeService nodeService;

	@Autowired
	@Qualifier("nodeLocatorService")
	NodeLocatorService nodeLocatorService;

	@Test
	public void testDocumentRenameWithAlfrescoContent() {
		AuthenticationUtil.setFullyAuthenticatedUser(AuthenticationUtil.getAdminUserName());

		// Récupération du dossier où classer le document
		NodeRef companyHome = nodeLocatorService.getNode("companyhome", null, null);
		assertNotNull(companyHome);

		// création du document de type contenu
		String documentName = "MonDocument" + UUID.randomUUID();
		FileInfo documentInfo = this.fileFolderService.create(companyHome, documentName, ContentModel.TYPE_CONTENT);
		assertNotNull(documentInfo);
		
		// Vérification du nom
		String newDocumentName = (String) this.nodeService.getProperty(documentInfo.getNodeRef(), ContentModel.PROP_NAME);
		assertNotNull(newDocumentName);
		
		// Le document est de type contenu, il ne doit pas être modifié
		assertEquals(documentName, newDocumentName);
	}
	

	@Test
	public void testDocumentRenameWithFormationContent() {
		AuthenticationUtil.setFullyAuthenticatedUser(AuthenticationUtil.getAdminUserName());

		// Récupération du dossier où classer le document
		NodeRef companyHome = nodeLocatorService.getNode("companyhome", null, null);
		assertNotNull(companyHome);

		// création du document de type contenu
		String documentName = "MonDocument" + UUID.randomUUID();
		FileInfo documentInfo = this.fileFolderService.create(companyHome, documentName, DocubaseModel.TYPE_CUSTOM_CONTENT);
		assertNotNull(documentInfo);
		
		// Vérification du nom
		String newDocumentName = (String) this.nodeService.getProperty(documentInfo.getNodeRef(), ContentModel.PROP_NAME);
		assertNotNull(newDocumentName);
		
		// Le document est de type "custom content", il doit être renommé
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = sdf.format(new Date());
		assertEquals(currentDate + "-" + documentName, newDocumentName);
	}
}
