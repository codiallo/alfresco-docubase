if (typeof Amexio == "undefined" || !Amexio)
{
   var Amexio = {};
}
Amexio.formation = Amexio.formation || {};
Amexio.formation.dashlet = Amexio.formation.dashlet || {};

(function()
{
   Amexio.formation.dashlet.MesDerniersDocuments = function MesDerniersDocuments(htmlId)
   {
      return Amexio.formation.dashlet.MesDerniersDocuments.superclass.constructor.call(this, htmlId);
   };

   YAHOO.extend(Amexio.formation.dashlet.MesDerniersDocuments, Alfresco.component.SimpleDocList,
   {
      onReady: function MyDocuments_onReady()
      {
         Amexio.formation.dashlet.MesDerniersDocuments.superclass.onReady.apply(this, arguments);
      },
      getWebscriptUrl: function SimpleDocList_getWebscriptUrl()
      {
         return Alfresco.constants.PROXY_URI + "formation/mesdesrniersdocuments";
      }
   });
})();
